package com.example.roomtest

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface Dao {
    @Insert
    fun insertProcessors(processors: Processors)
    @Query("SELECT * FROM Processors")
    fun getAllProcessors(): Flow<List<Processors>>
}
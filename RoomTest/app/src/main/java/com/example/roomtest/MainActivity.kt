
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import androidx.lifecycle.asLiveData
import com.example.roomtest.databinding.ActivityMainBinding
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.asLiveData

class MainActivity() : AppCompatActivity(), Parcelable {
    lateinit var binding: ActivityMainBinding

    constructor(parcel: Parcel) : this() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val db = MainDb.getDb(this)
        db.getDao().getAllProcessors().asLiveData().observe(this){ list->
            binding.tvList.text = ""
            list.forEach {
                val text = "Id: ${it.id} Motherboards: ${it.name} Series: ${it.price}\n"
                binding.tvList.append(text)
            }
        }
        binding.button2.setOnClickListener {
            val item = Processors(
                null,
                binding.edName.text.toString()
            )
            Thread{
                db.getDao().insertProcessors(Processors)
            }.start()
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MainActivity> {
        override fun createFromParcel(parcel: Parcel): MainActivity {
            return MainActivity(parcel)
        }

        override fun newArray(size: Int): Array<MainActivity?> {
            return arrayOfNulls(size)
        }
    }
}
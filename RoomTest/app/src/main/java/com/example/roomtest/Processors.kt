package com.example.roomtest
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

class Processors {
    @Entity (tableName = "processors")
    data class Processors(
        @PrimaryKey(autoGenerate = true)
        var id: Int? = null,
        @ColumnInfo(name = "motherboards")
        var motherboards: String,
        @ColumnInfo(name = "series")
        var series: String,
    )

}